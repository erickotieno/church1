<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use HasFactory;

    protected $fillable=[
        'husb_phone',
        'wife_phone',
        'child1',
        'child2',
        'child3',
        'child4',
        'child5',
        'child6',
        'child7',
        'child8',
        'child9',
        'child10',
    ];
}

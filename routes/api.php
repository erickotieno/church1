<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\MembersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@login');
Route::get('/viewuser/{id}', 'App\Http\Controllers\UserController@viewuser');
Route::get('allcouples','App\Http\Controllers\UserController@allcouples');
Route::get('grouplist', 'App\Http\Controllers\UserController@allgroups');
Route::group([
    'middleware' => 'auth:api'
], function() {

    //user
    Route::delete('/user/delete/{id}/', 'App\Http\Controllers\UserController@deluser');
    Route::get('allusers', 'App\Http\Controllers\UserController@allusers');
    // Route::get('/viewuser/{id}', 'App\Http\Controllers\UserController@viewuser');
    Route::get('/storage/image/');
    Route::get('/logout','App\Http\Controllers\UserController@logout');

    //groups
    Route::get('allgroups', 'App\Http\Controllers\UserController@allgroups');
    Route::get('/editgroup/{id}', 'App\Http\Controllers\UserController@editgroup');
    Route::put('/group/update/{id}','App\Http\Controllers\UserController@updateGroup');
    Route::delete('/group/delete/{id}','App\Http\Controllers\UserController@delgroup');
    Route::post('/addgroup', 'App\Http\Controllers\UserController@addgroup');
    Route::get('/{any}', 'App\Http\Controllers\UserController@home')->where('any', '.*');

    //couples
    Route::post('/regcouple', 'App\Http\Controllers\UserController@regcouple');
    // Route::get('/allcouples','App\Http\Controllers\UserController@allcouples');
});
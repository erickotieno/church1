<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->id();
            $table->string('wife_phone');
            $table->string('husb_phone')->unique();
            $table->string('child1')->nullable();
            $table->string('child2')->nullable();
            $table->string('child3')->nullable();
            $table->string('child4')->nullable();
            $table->string('child5')->nullable();
            $table->string('child6')->nullable();
            $table->string('child7')->nullable();
            $table->string('child8')->nullable();
            $table->string('child9')->nullable();
            $table->string('child10')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
